﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AzureTestPerformance.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AzureTestPerformance.Controllers {

    public class ConvertedTestResult {
        public string ServerName { get; set; }
        public List<MyWebResponse> TestResult { get; set; }
    }

    public class ValuesController : ApiController {
        private  WebRequestThreads tester = new WebRequestThreads();


      
        public JObject Get() {
            List<MyWebResponse> webRequestResult = tester.RunTest();
            string resultStr = JsonConvert.SerializeObject(new ConvertedTestResult {
                TestResult = webRequestResult,
                ServerName = Request.Headers.Host
            }
            ); // the list needs an anchor
            return JObject.Parse(resultStr);
        }




        [HttpGet]
        public JObject Test(string id) {
             
            return JObject.Parse(id);
        }



        // POST api/values
        public void Post([FromBody]string value) {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value) {
        }

        // DELETE api/values/5
        public void Delete(int id) {
        }
    }
}
