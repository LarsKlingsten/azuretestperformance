﻿using System;
using System.Linq;

namespace AzureTestPerformance.Models {
    public static class MyExtensions {

        public static string Left(this string str, int stringLength) {
            if (str == null || stringLength <= 0) { return ""; }
            if (stringLength > str.Length) { stringLength = str.Length; }
            return str.Substring(0, stringLength);
        }

        public static string Time(this string str) {
            return (string.Format("{0} {1}", System.DateTime.Now.ToString("hh:mm:ss.fff"), str));
        }

        public static string TimeUtc(this string str) {
            return (string.Format("{0} {1}", "".Time(), str));
        }

        public static void ToConsoleWithTime(this string str) {
            Console.WriteLine(string.Format("{0} {1}", "".Time(), str));
        }

        public static void ToDebugWithTime(this string str) {
            System.Diagnostics.Debug.WriteLine(string.Format("{0} {1}", System.DateTime.Now.ToString("hh:mm:ss.fff"), str));
        }

        public static string RemoveDigits(this string str) {
            return new string(str.Where(char.IsLetter).ToArray()).ToUpper();
        }
        public static string RemoveNonDigits(this string str) {
            return new string(str.Where(char.IsDigit).ToArray()).ToUpper();
        }
    }

}