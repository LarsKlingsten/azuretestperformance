﻿using System;
using System.Collections.Generic;

namespace AzureTestPerformance.Models {

    public class MyWebRequest {
        public Uri PageUri { get; set; }
        public int NumberRequests { get; set; }
        public string ApiKey { get; set; }

        public static MyWebRequest[] GetTestSomeData() {

            MyWebRequest[] myRequests = {
              new MyWebRequest { PageUri = new Uri("https://apiclient4.azurewebsites.net/api/front?s=StoreProduct&key=lars&q=test"), NumberRequests = 100,ApiKey = "" },
              new MyWebRequest { PageUri = new Uri("https://apiclient4.azurewebsites.net/api/front?s=StoreProduct&key=lars&q=test"), NumberRequests = 1,ApiKey = "" },
              new MyWebRequest { PageUri = new Uri("https://apiclient4.azurewebsites.net/api/front?s=StoreProduct&key=lars&q=test"), NumberRequests = 5,ApiKey = "" },
              new MyWebRequest { PageUri = new Uri("https://apiclient4.azurewebsites.net/api/front?s=StoreProduct&key=lars&q=test"), NumberRequests = 100,ApiKey = "" },
              new MyWebRequest { PageUri = new Uri("https://apiclient4.azurewebsites.net/api/front?s=StoreProduct&key=lars&q=test"), NumberRequests = 1000,ApiKey = "" },
          //     new MyWebRequest { PageUri = new Uri("https://apiclient4.azurewebsites.net/api/front?s=StoreProduct&key=lars&q=test"), NumberRequests = 100000,ApiKey = "" }
            };
            return myRequests;
        }
    }

    public class MyWebResponse {

        public string Url { get; set; }
        public decimal ElapsedMS { get; set; }
        public int NumberRequests { get; set; }
        public List<string> Items { get; set; }

        public MyWebResponse() {
            Items = new List<string>();
        }
    }
}