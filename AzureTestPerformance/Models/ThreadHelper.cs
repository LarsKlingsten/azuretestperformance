﻿using System;
using System.Threading;

namespace AzureTestPerformance.Models {
    public class MultiThreadHelper {
        private int _threadCount;
        private Object myLock = new Object();

        public void SetThreadCount(int count) {
            _threadCount = count;
        }

        public void ThreadDecreaseCount() {
            lock (myLock) {
                _threadCount--;
            }
        }

        public void AwaitCompletion() {
            while (_threadCount != 0) {
                Thread.Sleep(1);
            }
        }
    }
}