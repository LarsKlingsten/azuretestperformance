﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureTestPerformance.Models {

    public class WebRequestThreads {

        MultiThreadHelper myMultiThreadHelper = new MultiThreadHelper();
        private Dictionary<string, int> _resultDictionary;
        private List<MyWebResponse> myResponses = new List<MyWebResponse>();

        public List<MyWebResponse> RunTest() {
            MyWebRequest[] myRequests = MyWebRequest.GetTestSomeData();
            var sw = new Stopwatch();
        
            foreach (MyWebRequest service in myRequests) {
                _resultDictionary = new Dictionary<string, int>();
                sw.Restart();
                CallServicesViaThreadPool(service);
                sw.Stop();
                decimal millisec = decimal.Divide(sw.ElapsedTicks * 1000, Stopwatch.Frequency);
                myResponses.Add(new MyWebResponse { Url = service.PageUri.ToString(), ElapsedMS = millisec, NumberRequests = service.NumberRequests, Items = WriteResultDictionary() });
            }
            return myResponses;
        }

        public void CallServicesViaThreadPool(MyWebRequest request) {
            int MAXTHREADS = 128;

            if (request.NumberRequests < 3) { // Do not use threads as is not necessary, and is (maybe) slower for a single request
                for (int i = 0; i < request.NumberRequests; i++) {
                    GetWebResponse(request);
                }
            } else {
                myMultiThreadHelper.SetThreadCount(request.NumberRequests);
                ServicePointManager.DefaultConnectionLimit = MAXTHREADS;
                ThreadPool.SetMaxThreads(MAXTHREADS / 2, MAXTHREADS);
                for (int i = 0; i < request.NumberRequests; i++) {
                    Task.Run(() => GetWebResponse(request));
                }
                myMultiThreadHelper.AwaitCompletion();
            }
        }

        public string GetWebResponse(MyWebRequest r) {
            string result;
            WebRequest webRequest = WebRequest.Create(r.PageUri);
            webRequest.Proxy = null;
            HttpWebRequest httpWebRequest = (HttpWebRequest)webRequest;
            NetworkCredential myNetworkCredential = new NetworkCredential(r.ApiKey, "");
            CredentialCache myCredentialCache = new CredentialCache();
            myCredentialCache.Add(r.PageUri, "Basic", myNetworkCredential);
            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Credentials = myCredentialCache;

            try {
                WebResponse myWebResponse = webRequest.GetResponse();
                HttpWebResponse httpWebResponse = (HttpWebResponse)myWebResponse;
                Stream responseStream = myWebResponse.GetResponseStream();
                StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8);
                string htmlBody = streamReader.ReadToEnd();
                responseStream.Close();
                myWebResponse.Close();
                result = string.Format("response={0} msg={1}", httpWebResponse.StatusCode.ToString(), htmlBody.Left(80));
            } catch (WebException e) {
                result = e.Message.ToString().Left(40);
            }
            addResultToDictionary(result);
            myMultiThreadHelper.ThreadDecreaseCount();
            return result;
        }
        
        private object myLock = new object();
        public void addResultToDictionary(string httpCode) {
            int exitingResponse = -1;
            lock (myLock) {
                _resultDictionary.TryGetValue(httpCode, out exitingResponse);
                if (exitingResponse == -1) {

                } else {
                    _resultDictionary[httpCode] = ++exitingResponse;
                }
            }
        }

        private List<string> WriteResultDictionary() {
            return null;

            int count = 0;
            List<string> result = new List<string>();
            foreach (var entry in _resultDictionary) {
                result.Add(string.Format("count={0}  {1} ", entry.Value, entry.Key));
                count += entry.Value;
            }
            return result;
        }
    }
}